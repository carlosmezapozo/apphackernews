import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
 
@Injectable({providedIn: 'root'})
export class HelperService {
 
  url = "http://localhost:3000/api";
 
  constructor(private http: HttpClient) {
    
  }

  getArticle() {
    return this.http.get(this.url+'/articles')
  }

  removeArticle(id) {
    return this.http.put(this.url+'/articles/'+id ,{"_id": id } )
	.subscribe(
		data   => { console.log("PUT Request is successful ", data);},
		error  => { console.log("Error", error);}
	);
  }


}