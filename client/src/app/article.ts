//article.ts
export interface Article {
	_id: String,
	title: String,
	url: String,
	author: String,
	objectID: String,
	deleted: Boolean,
	created_at : Date
}
