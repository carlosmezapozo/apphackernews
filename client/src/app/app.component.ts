//app.component.ts
import { Component } from '@angular/core';
import { HelperService } from './_service/helper.service';
import {Article} from './models/article'
import {HttpClient} from '@angular/common/http';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[HelperService]
})

export class AppComponent {

  title = 'angular-7-listing-example';
  articles : Article[];
 
  constructor(public http: HttpClient, private helperService: HelperService){
  }
 
  ngOnInit(): void {
    this.getArticles();
  }

  getArticles() {
    this.helperService
    .getArticle()
    .subscribe(( data : {status  : String, message : String, data : Article[]} ) => {
      this.articles = data.data;

    });
  }
    
  remove(id) {

    this.helperService.removeArticle(id);
   
    for(let i = 0; i < this.articles.length; ++i){
        if (this.articles[i]._id === id) {
            this.articles.splice(i,1);
        }
    }

  }

  openNewTab(url){
    window.open(url, '_blank');
  }
}

