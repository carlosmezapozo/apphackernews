# appHackerNews

NodeJS Service
MongoDB 
Angular Cli


## clone the project
```bash
git clone https://gitlab.com/carlosmezapozo/apphackernews.git
```
## build the docker images
```bash
docker-compose build
```
## run the app
```bash
docker-compose up
```

## WE ARE READY !!
### Client on  
```bash
http://localhost:8080
```
### Server API on  
```bash
http://localhost:3000/api/
```


## To force first DBs populate go to 
```bash
http://localhost:3000/api/articles/forceupdate
```