// Filename: api-routes.js
// Initialize express router
let router = require('express').Router();

// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to RESTHub crafted with love!'
    });
});


// Import article controller
var articleController = require('./controllers/articleController');

// Article routes
router.route('/articles')
    .get(articleController.index);

router.route('/articles/forceupdate')
    .get(articleController.forceUpdate);
  

router.route('/articles/:article_id')
    .put(articleController.update);

// Export API routes
module.exports = router;





