var mongoose = require('mongoose');

// Setup schema
var articleSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true

    },
    created_at: {
        type: Date,
        default: Date.now
    },
    author: {
        type: String,
        required: true
    },
    objectID: {
        type: String,
        required: true,
        dropDups: true,
        unique: true
    }, 
    deleted: {
        type: Boolean,
        default: false
    }


});

module.exports = Article = mongoose.model('Article', articleSchema);

module.exports.get = function (callback, limit) {
   Article.find( {deleted : false}, callback ).limit(limit);
}