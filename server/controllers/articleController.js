// articleController.js
// Import article model
Article = require('../models/articleModel');

const request = require('request');

// Handle index actions
exports.index = function (req, res) {
    Article.get(function (err, articles) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "articles retrieved successfully",
            data: articles
        });
    });
};

// Handle update article info
exports.update = function (req, res) {
    Article.findById(req.params.article_id, function (err, article) {

        if (err)
            res.send(err);

        article.deleted = true;

// save the article and check for errors
        article.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'article deleted',
                data: article
            });
        });
    });
};


// Handle create article actions
exports.UpdateFromServer = function () 
{
    console.log("Updating from Url " + sourceUrl );  

    var jsonObject;
    request(sourceUrl, function (error, response, body) {
        if (error) {
            console.log(jsonObject);
            return;
        }

        if (!error && response.statusCode == 200) {
            jsonObject = JSON.parse(body);

            console.log("Articles :" + jsonObject.hits.length);

            jsonObject.hits.forEach(function(aux) {

                if( aux.story_title == null && aux.title == null ) {
                   console.log("Discarted by null title objectID : " + aux.objectID );
                   return;
                }
                // some articles has both null
                if( aux.story_url == null && aux.url == null ) {
                   console.log("Discarted by null url objectID : " + aux.objectID );
                   return;
                }

                var articleAux = new Article();

                articleAux.title = (aux.story_title != null )? aux.story_title : aux.title;
                articleAux.author = aux.author;
                articleAux.url = (aux.url != null )? aux.url : aux.story_url ;
                articleAux.objectID = aux.objectID;

                articleAux.save(function (err) {
                    if (err)
                        console.log("Error for objectID : " + aux.objectID + " msg : " + err);
                    else 
                        console.log("Article inserted " + aux.objectID );
                });


            });   
        }

    });


};

exports.forceUpdate = function (req,res) {
        exports.UpdateFromServer();

        res.json({
            status: "success",
            message: "articles retrieved successfully",
        });
};







