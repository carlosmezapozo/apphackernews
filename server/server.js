
const express = require("express");
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cron = require('node-cron');
const request = require('request');

require('console-stamp')(console, '[HH:MM:ss.l]');


// SOURCE URL TO READ ARTICLES
global.sourceUrl = "https://hn.algolia.com/api/v1/search_by_date?query=nodejs"

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//headers
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET,PUT");
  next();
});

// Import routes
let apiRoutes = require("./api-routes")

// Use Api routes in the App
app.use('/api', apiRoutes)

// Connect to Mongoose and set connection variable
//mongoose.connect('mongodb://localhost/apiArticles', { useNewUrlParser: true});
mongoose.connect('mongodb://mongo:27017/apiArticles', {useUnifiedTopology: true, useNewUrlParser: true});
var db = mongoose.connection;


// Added check for DB connection
if(!db)
    console.log("Error connecting db")
else
    console.log("Db connected successfully")

// Setup server port
var port = process.env.PORT || 3000;

// Send message for default URL
app.get('/', (req, res) => res.send('Running default on ' + port));

// Import article controller
var articleController = require('./controllers/articleController');

// CRON for Update Articles
cron.schedule('0 * * * *', () => {
  console.log('Running a task every hour');
  articleController.UpdateFromServer();
});

app.listen(3000, () => {
 console.log("Running on port " + port );
});	 
